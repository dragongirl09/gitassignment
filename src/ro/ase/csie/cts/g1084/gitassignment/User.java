package ro.ase.csie.cts.g1084.gitassignment;

import java.util.Map;
import java.util.Map.Entry;

public class User implements Facturabil {
	public Map<String, Double> valoriFacturi;
	public String nume;
	
	public User(Map<String, Double> valoriFacturi, String nume) {
		super();
		this.valoriFacturi = valoriFacturi;
		this.nume = nume;
	}

	@Override
	public double calculeazaValoareFacturi(Map<String, Double> facturi) {
		double suma = 0;
		for(Entry<String, Double> entry : facturi.entrySet()) {
			suma += entry.getValue();
		}
		return suma;
	}


}
