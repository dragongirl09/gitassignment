package ro.ase.csie.cts.g1084.gitassignment;

import java.util.Map;

public interface Facturabil {
	public double calculeazaValoareFacturi(Map<String, Double> facturi);
}
