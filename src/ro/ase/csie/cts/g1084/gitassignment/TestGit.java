package ro.ase.csie.cts.g1084.gitassignment;

import java.util.HashMap;
import java.util.Map;

public class TestGit {
	public static void main(String[] args) {
		System.out.println("Hello Git ! Denumirea proiectului de licenta este Aplicatie web pentru gestiunea obiectelor personale corelata cu bugetul disponibil");
	
		Map<String, Double> facturi = new HashMap<>();
		facturi.put("Internet", 22.0);
		facturi.put("Enel", 55.78);
		facturi.put("Digi", 40.30);
		
		User user1 = new User(facturi, "Gigica");
		
		double total = user1.calculeazaValoareFacturi(user1.valoriFacturi);
		System.out.println(total);
	}
}
